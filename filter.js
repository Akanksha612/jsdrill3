function filter(elements, cb) 
{
    if(elements==undefined || elements.length==0)
     return [];

     var count=0;
     var arr= new Array();

    for(var i=0;i<elements.length;i++)
    {
        var result= cb(elements[i],i,elements);

        if(result===true)
        {
            arr.push(elements[i]);
            count++;

        }

    }


    if(count==0)
    return [];

    else
    return arr;


}


module.exports=filter;